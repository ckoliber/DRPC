# Getting Started

Now we are going to decribe the project

## Author

ChaM author is [KoLiBer](mailto://koliberr136a1@gmail.com).

## Developers

1. [KoLiBer](mailto://koliberr136a1@gmail.com)
2. [KoLiBer](mailto://koliberr136a1@gmail.com)
3. [KoLiBer](mailto://koliberr136a1@gmail.com)
4. [KoLiBer](mailto://koliberr136a1@gmail.com)

## Date

ChaM started at **Tuesday, May 23, 2018** UTC date.

## Description

ChaM or **Chapar Messenger** is a live chat cross platform application and abstracted chatting protocol.

ChaM have three base parts :

1. **Server**: That stores informations and interacts with DBMS and manages users.
2. **Interface Application**: The User Interface for managing informations.

## License

ChaM is under **MIT** license. more informations about **MIT License** is [here](LICENSE.md)
![MIT License](https://pre00.deviantart.net/4938/th/pre/f/2016/070/3/b/mit_license_logo_by_excaliburzero-d9ur2lg.png)

## Languages

1. [HTML5]()
2. [CSS3]()
3. [JavaScript-ES6]()
4. [Node.JS]()

## Dependencies

1. [Git]()
2. [Docker]()
3. [Python]()
4. [Node.JS]()
5. [Python]()

## Build
