const { TCP } = require('./io/net/tcp');
const EventEmitter = require('events');

class DRPC extends EventEmitter {

    Start(port, sname, password) {
        this.tcp = new TCP();
        this.tcp.init.call(this.tcp, this, port, sname, password);
        this.tcp.start.call(this.tcp);
    }

    Stop() {
        this.tcp.stop.call(this.tcp);
    }

    Connect(host, port, password) {
        this.tcp.connect.call(this.tcp, host, port, password);
    }

    Disconnect(sname) {
        this.tcp.disconnect.call(this.tcp, sname);
    }

    List() {
        let result = {};
        for (let socket of Object.values(this.tcp.list)) {
            result[socket.sname] = socket;
        }
        return result;
    }

    Broadcast(message) {
        return this.tcp.broadcast.call(this.tcp, message);
    }

    Multicast(snames, message) {
        return this.tcp.multicast.call(this.tcp, snames, message);
    }

    Unicast(sname, message) {
        return this.tcp.unicast.call(this.tcp, sname, message);
    }

}

exports.DRPC = DRPC;