test('DRPC Test', async (done) => {
    const { DRPC } = require('../src/app');

    let drpc1 = new DRPC();
    let drpc2 = new DRPC();

    drpc1.on('start', () => {
        console.log(`DRPC1 Started`);
    });
    drpc1.on('stop', () => {
        console.log(`DRPC1 Stopped`);
    });
    drpc1.on('error', (error) => {
        console.log(`DRPC1 Errored: ${error}`);
    });
    drpc1.on('connect', (name) => {
        console.log(`DRPC1 Connected: ${name}`);
        drpc1.Broadcast.call(drpc1, 'MESSAGE FROM drpc1');
    });
    drpc1.on('disconnect', (name) => {
        console.log(`DRPC1 Disconnected: ${name}`);
    });
    drpc1.on('cast', (name, message) => {
        console.log(`DRPC1 Cast: ${name} -> ${message}`);
    });
    drpc2.on('start', () => {
        console.log(`DRPC2 Started`);
    });
    drpc2.on('stop', () => {
        console.log(`DRPC2 Stopped`);
    });
    drpc2.on('error', (error) => {
        console.log(`DRPC2 Errored: ${error}`);
    });
    drpc2.on('connect', (name) => {
        console.log(`DRPC2 Connected: ${name}`);
        drpc2.Broadcast.call(drpc2, `MESSAGE from drpc2`);
        setTimeout(() => {
            drpc2.Broadcast.call(drpc2, `MESSAGE2 from drpc2`);
        }, 6000);
    });
    drpc2.on('disconnect', (name) => {
        console.log(`DRPC2 Disconnected: ${name}`);
    });
    drpc2.on('cast', (name, message) => {
        console.log(`DRPC2 Cast: ${name} -> ${message}`);
    });

    drpc1.Start.call(drpc1, 12345, 'a', '123');
    drpc2.Start.call(drpc2, 12346, 'b', '123');

    drpc1.Connect.call(drpc1, '127.0.0.1', 12346, '123');

    setTimeout(() => {
        drpc1.Stop.call(drpc1);
        drpc2.Stop.call(drpc2);
        done();
    }, 9000);
}, 10000);